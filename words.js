const ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];
const teens = ['', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen', 'twenty'];
const tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
function numbersToWords() {
   let resultArray = [];
   for(let i = 1; i <= 1000; i ++) {
       let digits = String(i);
       if (i <= 10) {
           resultArray.push([i] + ' ' + ones[i]);
       } else if (i > 10 && i <= 20) {
           resultArray.push([i] + ' ' + teens[i - 10]);
       } else if (i > 20 && i < 100) {
           let ten = Number(digits[0]);
           let one = Number(digits[1]);
           if (i % 10 === 0){
               resultArray.push([i] + ': ' + tens[ten])
           } else{
               resultArray.push([i] + ': ' + tens[ten] + '-' + ones[one])
           }
       } else if (i >= 100 && i < 1000) {
           let hundred = Number(digits[0]);
           let ten = Number(digits[1]);
           let one = Number(digits[2]);
           if (ten==0 && one==0){
               resultArray.push([i] + ': ' + ones[hundred] + ' hundred ');
           } else if (ten==1 && one>0){
               resultArray.push([i] + ': ' + ones[hundred] + ' hundred and ' + teens[one]);
           } else if (ten==1 && one==0){
               resultArray.push([i] + ': ' + ones[hundred] + ' hundred and ' + ones[one + 10]);
           } else if (i % 10 === 0){
               resultArray.push([i] + ': ' + ones[hundred] + ' hundred and ' + tens[ten]);
           }else {
               resultArray.push([i] + ': ' + ones[hundred] + ' hundred and ' + tens[ten] + '-' + ones[one]);
           }
       } else {
           resultArray.push(' one thousand!');
       }
   }
    return resultArray;
 }



let output = document.getElementById("output");
let span = document.createElement("span");
span.innerHTML = numbersToWords();
output.appendChild(span);
console.log(numbersToWords());